package com.ub.backend.article.repository;

import com.ub.backend.article.model.ArticleDoc;
import com.ub.core.base.repository.IRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import javax.annotation.Nullable;
import java.util.Date;

@Repository
public class ArticleRepository implements IRepository<ArticleDoc, ObjectId>{
    @Autowired private MongoTemplate mongoTemplate;
    @Override
    public ArticleDoc save(ArticleDoc articleDoc) {
        articleDoc.setUpdateAt(new Date());
        mongoTemplate.save(articleDoc);

        return articleDoc;
    }

    @Override
    public void remove(ObjectId objectId) {
        ArticleDoc doc = findById(objectId);
        if(doc != null){
            mongoTemplate.remove(doc);
        }
    }

    @Nullable
    @Override
    public ArticleDoc findById(ObjectId objectId) {
        return mongoTemplate.findById(objectId,ArticleDoc.class);
    }

    @Override
    public boolean exist(Criteria criteria) {
        return mongoTemplate.exists(Query.query(criteria),ArticleDoc.class);
    }
}
