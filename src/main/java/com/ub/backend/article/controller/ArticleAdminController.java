package com.ub.backend.article.controller;

import com.ub.backend.article.model.ArticleDoc;
import com.ub.backend.article.repository.ArticleRepository;
import com.ub.backend.article.routes.ArticleAdminRoutes;
import com.ub.core.base.utils.RouteUtils;
import com.ub.core.picture.model.PictureDoc;
import com.ub.core.picture.repository.PictureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ArticleAdminController {
    @Autowired private ArticleRepository articleRepository;
    @Autowired private PictureRepository pictureRepository;

    @GetMapping(ArticleAdminRoutes.ADD)
    public  String add(Model model){
        model.addAttribute("doc",new ArticleDoc());
        return "com.ub.backend.article.admin.add";
    }
    @PostMapping(ArticleAdminRoutes.ADD)
    public String add(@ModelAttribute("doc") ArticleDoc doc,
                      @RequestParam MultipartFile pic,
                      RedirectAttributes ra){
        PictureDoc pictureDoc = pictureRepository.save(pic);
        doc.setPicId(pictureDoc.getId());

        articleRepository.save(doc);
        ra.addAttribute("id",doc.getId());
        return RouteUtils.redirectTo(ArticleAdminRoutes.EDIT);
    }
}
