package com.ub.backend.article.routes;

import com.ub.core.base.routes.BaseRoutes;

public class ArticleAdminRoutes {
    public static final String ROOT = BaseRoutes.ADMIN+"/article";

    public static final String ADD = ROOT + "/add";
    public static final String EDIT = ROOT + "/edit";
    public static final String ALL = ROOT + "/all";
    public static final String REMOVE = ROOT + "/remove";
}
