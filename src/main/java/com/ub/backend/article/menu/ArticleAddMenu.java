package com.ub.backend.article.menu;

import com.ub.backend.article.routes.ArticleAdminRoutes;
import com.ub.core.base.menu.BaseMenu;

public class ArticleAddMenu extends BaseMenu {
    public ArticleAddMenu(){
        this.name="Добавить";
        this.parent=new ArticleMenu();
        this.url = ArticleAdminRoutes.ADD;
    }
}
