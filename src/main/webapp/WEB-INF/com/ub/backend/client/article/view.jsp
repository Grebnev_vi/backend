<section>
    <div class="container article-title">
        <div class="row">
            <h1 class="article-title-h1">
                Что нового в IOS 10
            </h1>
        </div>
    </div>
</section>
<section>
    <div class="container article-pic">
        <div class="row">
            <div class="col-md-12">
                <img class="article-pic-img" src="/static/backend/img/article-pic.jpg"/>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12 article-content">
                Крейг Федериги, вице-президент Apple по разработке программного обеспечения,
                предостерег спецслужбы от попыток принудить компанию к нарушению частной жизни
                своих клиентов даже в интересах следствия. Заявление топ-менеджера стало ответом
                на доклад The New York Times о намерении властей США принять ряд законопроектов,
                обязывающих производителей электроники предоставлять ФБР ключи взлома своей продукции.
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container article-comment-add">
        <div class="row">
            <div class="col-sm-1">
                <div class="article-comment-add-userpic"></div>
            </div>
            <div class="col-sm-10">
                <div class="article-comment-add-comment">
                    <input class="article-comment-add-comment-input">
                </div>
            </div>
            <div class="col-sm-1">
                <div class="article-comment-add-submit">
                    <button class="article-comment-add-submit-btn"></button>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container article-comment-list">
        <div class="row">
            <div class="col-md-12 article-comment-list-item">
                <div class="article-comment-list-userpic"></div>
                <div class="article-comment-list-conttent">
                    <div class="article-comment-list-conttent-date">
                        13 июня 2016
                    </div>
                    <div class="article-comment-list-conttent-text">
                        А вообще на андроид похоже
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>