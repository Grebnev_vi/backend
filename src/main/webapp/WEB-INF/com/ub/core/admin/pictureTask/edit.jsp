<%@ page import="com.ub.core.pictureTask.routes.PictureTaskAdminRoutes" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="section">
    <form:form action="<%= PictureTaskAdminRoutes.EDIT %>" class="card-panel" modelAttribute="doc" method="post"
               autocomplete="off">
        <h4 class="header2"><s:message code="ubcore.admin.form.editing"/></h4>

        <jsp:include page="/WEB-INF/com/ub/core/admin/components/errorAlert.jsp"/>

        <form:hidden path="id"/>

        <div class="row">
            <div class="input-field col l12">
                <label for="pictureId">Picture Id</label>
                <form:input path="pictureId" id="pictureId"/>
            </div>
        </div>

        <div class="row">
            <div class="input-field col l12">
                <label for="width">Width</label>
                <form:input path="width" id="width"/>
            </div>
        </div>

        <div class="row">
            <div class="input-field col l12">
                <label for="status">Status</label>
                <form:input path="status" id="status"/>
            </div>
        </div>

        <div class="row">
            <div class="input-field col l12">
                <label for="exc">Errors</label>
                <form:textarea path="exc" cssClass="materialize-textarea" id="exc"/>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <form:button class="btn cyan waves-effect waves-light right" type="submit" name="action">
                    <s:message code="ubcore.admin.form.submit"/><i class="mdi-content-send right"></i>
                </form:button>
            </div>
        </div>
    </form:form>
</div>