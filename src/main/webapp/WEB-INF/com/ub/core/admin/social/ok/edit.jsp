<%@ page import="com.ub.core.social.ok.routes.OkAdminRoutes" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="section">
    <form:form action="<%= OkAdminRoutes.SETTINGS_EDIT %>" class="card-panel" modelAttribute="doc" method="post"
               autocomplete="off">
        <h4 class="header2">Изменение настроек Odnoklassniki</h4>

        <jsp:include page="/WEB-INF/com/ub/core/admin/components/errorAlert.jsp"/>

        <form:hidden path="id"/>

        <div class="row">
            <div class="input-field col s12">
                <label for="CLIENT_ID">CLIENT_ID</label>
                <form:input path="CLIENT_ID" id="CLIENT_ID"/>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <label for="PUBLIC_KEY">PUBLIC_KEY</label>
                <form:input path="PUBLIC_KEY" id="PUBLIC_KEY"/>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <label for="REDIRECT_URI">REDIRECT_URI</label>
                <form:input path="REDIRECT_URI" id="REDIRECT_URI"/>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <label for="RESPONSE_TYPE">RESPONSE_TYPE</label>
                <form:input path="RESPONSE_TYPE" id="RESPONSE_TYPE"/>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <label for="SECRET_KEY">SECRET_KEY</label>
                <form:input path="SECRET_KEY" id="SECRET_KEY"/>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <form:button class="btn cyan waves-effect waves-light right" type="submit" name="action">
                    <s:message code="ubcore.admin.form.submit"/><i class="mdi-content-send right"></i>
                </form:button>
            </div>
        </div>
    </form:form>
</div>
