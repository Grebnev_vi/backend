<%@ page import="com.ub.core.user.model.UserStatus" %>
<%@ page import="com.ub.core.user.routes.UserAdminRoutes" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:set var="STATUS_ACTIVE" value="<%= UserStatus.ACTIVE %>"/>
<c:set var="STATUS_BLOCK" value="<%= UserStatus.BLOCK %>"/>

<div id="js-user-modal" class="modal modal-document-select">
    <div class="modal-content">
        <h4>Select user</h4>

        <div class="row">
            <div class="input-field col s9 m10 l11">
                <input id="js-user-modal-search-query" type="text" placeholder="Search"/>
            </div>

            <div class="input-field col s3 m2 l1">
                <div class="input-field">
                    <button id="js-user-modal-search-button" class="btn-floating waves-effect waves-light right" type="submit">
                        <i class="mdi-action-search"></i>
                    </button>
                </div>
            </div>
        </div>

        <div id="js-user-modal-content">
        </div>
    </div>
</div>

<script>
    $(function () {
        function init() {
            $.get(
                "<%= UserAdminRoutes.MODAL_RESPONSE %>",
                {
                    query: $('#js-user-modal-search-query').val()
                },
                updateContent
            );
        }

        function updateContent(data) {
            $('#js-user-modal-content').html(data);
        }

        function onClickItem() {
            var id = $(this).attr('data-id');
            var title = $(this).attr('data-title');
            $('#js-user-input-id').val(id);
            $('#js-user-input-title').val(title);
            $('#js-user-modal').closeModal();
        }

        function onClickPage() {
            $.get(
                "<%= UserAdminRoutes.MODAL_RESPONSE%>",
                {
                    query: $(this).data('query'),
                    currentPage: $(this).data('page')
                },
                updateContent
            );
        }

        $('#js-user-button-select').click(function () {
            $('#js-user-modal').openModal();
            init();
            return false;
        });

        $('#js-user-button-clear').click(function () {
            $('#js-user-input-id').val('');
            $('#js-user-input-title').val('');
            return false;
        });

        $('#js-user-modal').on('click', '.js-modal-table-item', onClickItem);
        $('#js-user-modal').on('click', '.js-modal-getPagination', onClickPage);
        $('#js-user-modal-search-button').click(init);
    });
</script>